import hug


@hug.get('/', output=hug.output_format.json)
def hello_world():
    """
    Simple method to return a JSON formatted 'Hello World' response.
    """
    return {'message': 'Hello World!'}
